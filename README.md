#Titolo
KICK-OUT!
##Sviluppato con 
* LibGDX

##Autori
 - Carit� Davide
 - Gallala Ramzi
 - Lama Matteo
 - Benini Luca

##Istruzioni
Scaricare ed eseguire il file KICK-OUT!.jar. Assicurarsi che sia installata sul dispositivo una JRE funzionante.

##Guida utente
Il gioco parte con un' interfaccia in cui i giocatori potranno inserire i nicknames, che sono obbligatori, e scegliere i personaggi e lo sfondo.  Una volta premuto "Play Now!!" il gioco parte.
Gli utenti controllano i caratteri attraverso la tastiera.
I giocatori si sfidano muovendosi e saltando per lo schermo.
L'arena � formata da una piattaforma raffigurata come una specie di iceberg sospeso nel vuoto dove  potr�  apparire un trappola che se viene attivata rende il rivale pi� lento.
Nel gioco i due personaggi partecipano ad un combattimento dove possono saltare, attaccare e far si che l'avversario finisca fuori dallo schermo.
La partita dura fino a che uno dei due giocatori non finisce fuori dallo schermo. Alla fine si vede una schermata che mostra il nome del giocatore perdente e due bottoni, uno per uscire del gioco e l'altro che riporta al men� iniziale.
##Licenza
*MIT