package utility;
/**
 * Used to contain constants used throughout the entire application
 */
public final class Constants {
	/**
	 * Density Of Pixels, used to establish how many pixels there are in the currently used unity of measure
	 */
    public static final int DOP = 5;
    /**
     * The standard width of a Fighter
     */
    public static final float WIDTH_FIGHTER = 10;
    /**
     * The standard height of a Fighter
     */
    public static final float HEIGHT_FIGHTER = 15;
}
